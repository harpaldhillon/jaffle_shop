FROM ubuntu:latest
MAINTAINER "harpal.dhillon@hotmail.com"

RUN apt-get update && apt-get install git libpq-dev python3-dev python3-pip -y

RUN pip3 install dbt && pip3 install git+https://github.com/mikaelene/dbt-test-coverage.git

ENTRYPOINT ["dbt"]
CMD ["--version"]
